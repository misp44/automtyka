from flask import Flask
from app.extensions import *


def create_app(config_object='app.settings.Config'):
    app = Flask(__name__)

    app.config.from_object(config_object)

    cors(app, resources={r'/*': {'origins': '*'}})

    from app.handlers import handlers

    app.register_blueprint(handlers)

    return app

