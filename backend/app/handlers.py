from flask import Blueprint, jsonify, request
from app.simulation.simulation import Simulation
from app.simulation.model.wall import WallLayer, Wall
from app.simulation.model.room import Room
from app.simulation.model.radiator import Radiator
from app.simulation.model.air import Air
from app.simulation.model.outdoor import Outdoor

from app.simulation.measurements import Measurements

from app.simulation.controllers.PID import PIDController

from threading import Thread

handlers = Blueprint('handlers', __name__)


@handlers.route('/api/run', methods=['GET', 'POST'])
def run():
    response_object = {'status': 'success', 'message': 'started simulation'}
    if request.method == 'POST':
        simulation_data = request.get_json()
        t = Thread(target=run_simulation, args=(simulation_data, ))
        t.start()
    else:
        response_object['simulation'] = "ok"
    return jsonify(response_object)


def run_simulation(data):
    simulation = parse_body(data)
    simulation.run(data['time'] * 3600)


def parse_body(body):
    outdoor = parse_outdoor(body['outdoor'])
    dimensions = parse_dimensions(body['room'])
    air = parse_air(body['air'])
    room = Room(*dimensions, air)
    radiator = parse_radiator(body['radiator'])
    room.add_radiator(radiator)
    walls = parse_walls(body['walls'], outdoor, dimensions)
    [room.add_wall(wall) for wall in walls]
    controller = PIDController(air, radiator, *parse_controller(body['controller']))
    measurements = Measurements([air, room, controller, radiator], body['simulation_id'])
    simulation = Simulation([
        *outdoor,
        *walls,
        radiator,
        room,
        air,
        controller,
        measurements])

    return simulation


def parse_outdoor(body):
    return [Outdoor('outdoor', body['outdoor_temperatures']), Outdoor('ground', body['ground_temperatures'])]


def parse_controller(body):
    return [body['set_temperature'], body['proportional'], body['integral'], body['derivative']]


def parse_dimensions(body):
    return [body['width'], body['depth'], body['height']]


def parse_air(body):
    return Air(body['initial_temperature'], body['altitude'], body['humidity'])


def parse_walls(walls_data, outdoor_data, dimensions):
    outdoor, ground = outdoor_data
    width, depth, height = dimensions

    walls = [
        Wall('left', 'S', width, height, outdoor, parse_layers(walls_data['left'])),
        Wall('right', 'S', width, height, outdoor, parse_layers(walls_data['right'])),
        Wall('front', 'L', depth, height, outdoor, parse_layers(walls_data['front'])),
        Wall('back', 'L', depth, height, outdoor, parse_layers(walls_data['back'])),
        Wall('ceiling', 'C', width, depth, outdoor, parse_layers(walls_data['ceiling'])),
        Wall('floor', 'F', width, depth, ground, parse_layers(walls_data['floor'])),
    ]
    return walls


def parse_layers(layers):
    return [WallLayer(layer['conductivity'], layer['thickness']) for layer in layers]


def parse_radiator(body):
    inlet = body['inlet_temperature']
    return Radiator(
        'radiator',
        body['nominal_heat_emission'],
        inlet,
        inlet - 20 if inlet > 20 else 1,
        body['radiator_constant'])
