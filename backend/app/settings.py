import os


class Config:
    INFLUXDB_DATABASE = os.environ.get('INFLUX_DB')
    INFLUXDB_USERNAME = os.environ.get('INFLUX_ADMIN_USER')
    INFLUXDB_PASSWORD = os.environ.get('INFLUX_ADMIN_PASSWORD')
