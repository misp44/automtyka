import statistics
import time
from collections import namedtuple
from time import strftime, gmtime
from influxdb import InfluxDBClient
import os

class WrongUnitError(Exception):
    def __init__(self, expected, received):
        self.message = f'Expected unit {expected}, received ${received}'


class Measurements(object):

    def __init__(self, objects, sim_id):
        self._measurements = {}
        self._objects = objects
        self._data_point = namedtuple('DataPoint', ['Value', 'TimeStamp'])
        self._total_time = 0
        self._json_body = []
        self._db = self.init_db()
        self._sim_id = sim_id

    def init_db(self):
        influx = InfluxDBClient('127.0.0.1', 8086,
                                os.environ.get('INFLUXDB_ADMIN_USER'),
                                os.environ.get('INFLUXDB_USER_PASSWORD'),
                                'simulation')
        influx.create_database('simulation')
        return influx

    def calculate(self, timeStamp):
        for o in self._objects:
            for k, m in o.measurements.items():
                self._add_measurement(k, m, timeStamp)
                self._json_body.append(self.parse_to_json(k, m, timeStamp))

        self._total_time = timeStamp + 1

        if timeStamp % 100 == 0:
            self.write_to_influx()

    def parse_to_json(self, k, m, ts):
        return {
            "measurement": k,
            "tags": {
                "simulation_id": self._sim_id,
                "unit": m.Unit
            },
            "time": strftime("%Y-%m-%dT%H:%M:%SZ", gmtime(ts)),
            "fields": {
                "value": m.Value
            }
        }

    def write_to_influx(self):
        self._db.write_points(self._json_body)
        self._json_body = []

    def _add_measurement(self, key, m, timeStamp):
        if key not in self._measurements:
            # TODO: Javascriptish approach, probably not the best fit for Python
            self._measurements[key] = {
                'unit': m.Unit,
                'values': []
            }

        if self._measurements[key]['unit'] != m.Unit:
            raise WrongUnitError(self._measurements[key]['unit'], m.Unit)

        self._measurements[key]['values'].append(self._data_point(m.Value, timeStamp))

    def get_measurements(self, key):
        return self._measurements[key]

    def get_measurements_unit(self, key):
        return self._measurements[key]['unit']

    def get_measurements_values(self, key):
        return [dp.Value for dp in self._measurements[key]['values']]

    def print_stats(self):
        for k, v in self._measurements.items():
            values = [val.Value for val in v['values']]
            initial = values[0]
            final = values[-1]
            mean = statistics.mean(values)
            unit = v["unit"]

            print(
                f"{ k }:\n"
                f"\t Initial: { initial } [{ unit }]\n"
                f"\t Final:   { final } [{ unit }]\n"
                f"\t Mean:    { mean } [{ unit }]\n"
            )
        
        pretty_time = time.strftime('%H:%M:%S', time.gmtime(self._total_time))

        print(f"Simulation time: { pretty_time }")
