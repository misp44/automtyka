from app.simulation.controllers.controller import Controller


class PIDController(Controller):
    def __init__(self, input, output, setpoint, P, Ti, Td):
        super().__init__(input, output, setpoint)

        self._P = P
        self._Ti = Ti
        self._Td = Td

        self._sum_of_errors = 0
        self._last_err = 0

    def calculate_gain(self, input):
        err = self._setpoint - input

        # Integral
        self._sum_of_errors += err # TODO: * dt ?

        # Derivative
        d = err - self._last_err # TODO: / dt ?
        self._last_err = err

        return self._P * err + self._sum_of_errors / self._Ti + d * self._Td
