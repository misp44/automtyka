from app.simulation.measurement import Measurement


class ImpulseController(object):
	def __init__(self, input, output, amp):
		self._input = input
		self._output = output
		self._amp = amp
		self.measurements = {}

	def calculate(self, timestamp):
		out = self._amp if timestamp > 100 else 0

		self._output.set_gain(out)

		self.measurements = {
			"controller gain": Measurement(out, 'amplification') 
		}
