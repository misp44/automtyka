from app.simulation.measurement import Measurement
import numpy as np


class Radiator(object):

    def __init__(self, name, nominal_heat_emission, water_inlet_temp, water_outlet_temp, radiator_constant, initial_inlet_temp = None, initial_outlet_temp = None):
        self.name = name
        self._nominal_heat_emission = nominal_heat_emission
        self._in_T = water_inlet_temp
        self._out_T = water_outlet_temp
        self._const = radiator_constant
        self._gain = 0.1
        self.measurements = {}
        self._last_in_T = water_inlet_temp if initial_inlet_temp is None else initial_inlet_temp
        self._last_out_T = water_outlet_temp if initial_outlet_temp is None else initial_outlet_temp

        # For tests
        # self._last_in_T = 0
        # self._last_out_T = 0

    def calculate(self, timestamp):
        self.measurements = {
            "radiator gain": Measurement(self._gain, 'amplification')
        }

    # Equation B1
    def get_heat_emission(self, air_temp):
        time = 1 / (60 * 5)

        in_set_T = self._gain * (self._in_T - air_temp) + air_temp
        out_set_T = in_set_T * 0.7 + air_temp

        self._last_in_T = self._last_in_T + time * (in_set_T - self._last_in_T)
        self._last_out_T = self._last_out_T + time * (out_set_T - self._last_out_T)

        return Measurement(
            self._nominal_heat_emission *
            (
                (self._last_in_T - self._last_out_T) /
                np.log(
                    (self._last_in_T - air_temp) / (self._last_out_T - air_temp)
                ) *
                (1 / 49.32)
            ) ** self._const,
            "W"
        )

    def set_gain(self, gain):
        self._gain = 0.1 if gain < 0.1 else (1 if gain > 1 else gain)

    def get_gain(self):
        return self._gain

