# automatyka-backend
## Development
### Project setup
```
pipenv install
pipenv run pip freeze > requirements.txt
```

#### Run the server
```
pipenv shell
python run.py
```
##Production
```
docker build . -t backend
docker run -d -p 8080:8080 backend
```