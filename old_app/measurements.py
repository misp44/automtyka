import matplotlib.pyplot as plt
import statistics
import time
from collections import namedtuple


class WrongUnitError(Exception):
    def __init__(self, expected, received):
        self.message = f'Expected unit {expected}, received ${received}'


class Measurements(object):

    def __init__(self, objects):
        self._measurements = {}
        self._objects = objects
        self._data_point = namedtuple('DataPoint', ['Value', 'TimeStamp'])
        self._total_time = 0

    def calculate(self, timeStamp):
        for o in self._objects:
            for k, m in o.measurements.items():
                self._add_measurement(k, m, timeStamp)
        
        self._total_time = timeStamp + 1

    def _add_measurement(self, key, m, timeStamp):
        if key not in self._measurements:
            # TODO: Javascriptish approach, probably not the best fit for Python
            self._measurements[key] = {
                'unit': m.Unit,
                'values': []
            }

        if self._measurements[key]['unit'] != m.Unit:
            raise WrongUnitError(self._measurements[key]['unit'], m.Unit)

        self._measurements[key]['values'].append(self._data_point(m.Value, timeStamp))

    def get_measurements(self, key):
        return self._measurements[key]

    def get_measurements_unit(self, key):
        return self._measurements[key]['unit']

    def get_measurements_values(self, key):
        return [dp.Value for dp in self._measurements[key]['values']]

    def print_stats(self):
        for k, v in self._measurements.items():
            values = [val.Value for val in v['values']]
            initial = values[0]
            final = values[-1]
            mean = statistics.mean(values)
            unit = v["unit"]

            print(
                f"{ k }:\n"
                f"\t Initial: { initial } [{ unit }]\n"
                f"\t Final:   { final } [{ unit }]\n"
                f"\t Mean:    { mean } [{ unit }]\n"
            )
        
        pretty_time = time.strftime('%H:%M:%S', time.gmtime(self._total_time))

        print(f"Simulation time: { pretty_time }")

    def show(self):
        fig, axs = plt.subplots(len(self._measurements))

        fig.subplots_adjust(hspace=0.5)

        plot = 0

        for key, measurements in self._measurements.items():
            axs[plot].plot([val.Value for val in measurements['values']])
            axs[plot].set_title(key)
            axs[plot].set_ylabel('[%s]' % measurements['unit'])

            plot += 1

        plt.show()
