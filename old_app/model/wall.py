from measurement import Measurement

# values are thermal conductivities in [W/m*K]
# extracted from: https://thermtest.com/materials-database
materials = {
    'glass': 1.046,
    'air': 0.025,
    'spruce': 0.126,
    'concrete cinder': 0.335,
    'brick': 0.711,
    'mica insulator': 0.121,
    'ytong1L40': 0.575,
    'ytong2L24/15': 0.105,
    'drutexIgloLight': 1.1,
    'drutexIgloEnergy': 0.5,
}


class WallLayer(object):

    def __init__(self, thermal_conductivity, thickness):
        self.thickness = thickness
        self.thermal_conductivity = thermal_conductivity
        self.R = self._calculate_R_value()

    def _calculate_R_value(self):
        return Measurement(self.thickness / self.thermal_conductivity, "W/((m^2)*K)")


# Use C for ceiling, F for floor and L for vertical long with S for vertical short
class Wall(object):
    def __init__(self, name, position, width, height, outdoor, layers):
        self.layers = layers
        self.name = name
        self.type = position
        self.width = width
        self.height = height
        self.area = Measurement(width * height, "m^2")
        self.outdoor = outdoor
        self.outside_temp = 0

    def calculate(self, timestamp):
        self.outside_temp = self.outdoor.get_temperature().Value

    # Equation C1
    # Make sure the temp is in either C, F or K in all of these!
    def calculate_heat_transfer_over_time(self, air_temp):
        return Measurement((self.area.Value * (air_temp - self.outside_temp)) /
                           sum(layer.R.Value for layer in self.layers), "W")
