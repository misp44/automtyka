from measurement import Measurement
import statistics


class CannotAddWall(Exception):
    def __init__(self, wall):
        self.message = f'Cannot add wall {wall.name}'


class Room(object):

    def __init__(self, x, y, z, air):
        self._walls = {}
        self._X = x
        self._Y = y
        self._Z = z
        self._volume = self._calculate_volume()
        self._air = air
        self._radiators = {}
        self.measurements = {}

    def calculate(self, timestamp):
        walls_heat_transfer = self.get_total_heat_conductive_transfer_from_walls()
        radiators_heat_emission = self.get_total_heat_emission_from_radiators()

        self.measurements = {
            "walls heat transfer": walls_heat_transfer,
            "radiators heat emission": radiators_heat_emission,
            "surrounding temperature": self.get_mean_surrounding_temp()
        }

        temp_change = self.get_air_temperature_change(
            radiators_heat_emission.Value - walls_heat_transfer.Value
        )

        self.update_air_temp(temp_change.Value)

    def _calculate_volume(self):
        return Measurement(self._X * self._Y * self._Z, "m^3")

    def get_volume(self):
        return self._volume

    def add_wall(self, wall):
        if \
                (wall.type == 'L' and sum(wall.type == 'L' for wall in self._walls.values()) <= 2) or \
                        (wall.type == 'S' and sum(wall.type == 'S' for wall in self._walls.values()) <= 2) or \
                        (wall.type == 'C' and not any(wall.type == 'C' for wall in self._walls.values())) or \
                        (wall.type == 'F' and not any(wall.type == 'F' for wall in self._walls.values())):

            self._walls[wall.name] = wall
        else:
            raise CannotAddWall(wall)

    def update_air_temp(self, air_temp):
        self._air.update_temp_with_delta(air_temp)

    def add_radiator(self, radiator):
        self._radiators[radiator.name] = radiator

    def get_radiators(self):
        return self._radiators

    def get_total_heat_emission_from_radiators(self):
        return Measurement(sum(r.get_heat_emission(self._air.get_celsius_T()).Value
                               for r in self._radiators.values()), "W")

    def get_total_heat_conductive_transfer_from_walls(self):
        return Measurement(sum(wall.calculate_heat_transfer_over_time(self._air.get_celsius_T()).Value
                               for wall in self._walls.values()), 'W')

    def get_air_temperature_change(self, heat):
        air_mass = self.get_volume().Value * self._air.get_density().Value

        return Measurement(heat / (air_mass * self._air.heat_capacity.Value), "°C")

    def get_mean_surrounding_temp(self):
        return Measurement(statistics.mean(wall.outside_temp for wall in self._walls.values()), "°C")
