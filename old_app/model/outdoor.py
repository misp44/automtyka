from scipy.interpolate import interp1d
from measurement import Measurement
import numpy as np

# 3 hours interval
# max 48 hour of simulation

temperatures = [
    24,
    23,
    18,
    17,
    16,
    15,
    15,
    20,
    20,
    15,
    14,
    13,
    13,
    12,
    13,
    14,
]

ground_temperatures = [
    20,
    18,
    16,
    15,
    13,
    12,
    11,
    12,
    13,
    12,
    11,
    11,
    10,
    10,
    10,
    12,
]


class Outdoor(object):
    def __init__(self, name, temperatures):
        x = np.linspace(0, 48 * 3600, num=len(temperatures), endpoint=True)

        self._name = name
        self._temperatures = interp1d(x, temperatures, kind='cubic')
        self._temperature = Measurement(0, '°C')
        self.measurements = {}

    def calculate(self, timestamp):
        self._temperature = Measurement(np.asscalar(self._temperatures(timestamp)), '°C')

        self.measurements = {
            f"{self._name} temperature": self._temperature
        }

    def get_temperature(self):
        return self._temperature

class ConstantOutdoor(object):
    def __init__(self, temperature):
        self._temperature = Measurement(temperature, '°C')

    def calculate(self, timeStamp):
        pass

    def get_temperature(self):
        return self._temperature
