from measurement import Measurement
from scipy.constants import R
import numpy as np


# Note that temperature has to be dry bulb temp!
# We're assuming:
#   temperature --> C
#   humidity --> floating point (e.g: 40% == 0.4)
#   altitude --> meters above sea level
# TODO fix equations for zero values of the parameters
# TODO calcualte air heat capacity
def get_water_vapor_gas_constant():
    return Measurement(R / 18.02 * 1000, "J/(kg*K)")  # where 28.97 is molecular mas of air [g/kmol]


def get_air_gas_constant():
    return Measurement(R / 28.97 * 1000, "J/(kg*K)")  # where 28.97 is molecular mas of air [g/kmol]


class Air(object):

    def __init__(self, temperature, altitude, humidity):
        self._temperature = temperature
        self._altitude = altitude
        self._humidity = humidity
        self._pressure = self._calculate_pressure()
        self._dry_air_density = self._calculate_dry_air_denisty()
        self._density = self._calculate_density()
        self.heat_capacity = Measurement(1005, "J/kg")
        self.measurements = {}

    def calculate(self, timestamp):
        self.measurements = {
            "air temperature": Measurement(self._temperature, "°C")
        }

        pass

    def _calculate_density(self):
        if self._humidity != 0:
            return self._calculate_moist_air_density()
        return self._dry_air_density

    # Equation A1
    def _calculate_pressure(self):
        return Measurement(101325 * (1 - (2.25577 * (10 ** (-5)) * self._altitude)) ** 5.25588, "Pa")

    # Equation A2
    # Arden-Buck equations
    def _calculate_water_vapour_pressure(self):
        T = self.get_celsius_T()
        if T > 0:
            return Measurement(0.61121 * np.e ** ((18.678 - (T / 234.5)) *
                                                  (T / (T + 257.14))) * 1000, "Pa")
        return Measurement(0.6115 * np.e ** ((23.036 - (T / 333.7)) *
                                             (T / (T + 279.82))) * 1000, "Pa")

    # Equation A3
    def _calculate_dry_air_denisty(self):
        T = self.get_kelvin_T()
        air_gas_constant = get_air_gas_constant()
        return Measurement(self._pressure.Value / (air_gas_constant.Value * T), "kg/m^3")

    # Equation A4
    def _calculate_humid_air_density(self):
        T = self.get_kelvin_T()
        water_vapor_pressure = self._calculate_water_vapour_pressure()
        water_vapour_gas_constant = get_water_vapor_gas_constant()
        return Measurement(water_vapor_pressure.Value / (water_vapour_gas_constant.Value * T), "kg/m^3")

    # Equation A5
    def _calculate_water_vapor_saturation_pressure(self):
        T = self.get_kelvin_T()
        return Measurement(np.e ** (77.3450 + (0.0057 * T) - (7235 / T)) / (T ** 8.2), "Pa")

    # Equation A6
    def _calculate_water_vapor_saturation_mass_ratio(self):
        saturation_pressure = self._calculate_water_vapor_saturation_pressure().Value
        return Measurement(0.62198 * saturation_pressure / (self._pressure.Value - saturation_pressure), "kg/kg")

    def _calculate_water_vapor_actual_mass_ratio(self):
        return Measurement(self._calculate_water_vapor_saturation_mass_ratio().Value * self._humidity, "kg/kg")

    # Equation A7
    def _calculate_moist_air_density(self):
        gas_constant_ratio = get_water_vapor_gas_constant().Value / get_air_gas_constant().Value
        humidity_ratio = self._calculate_water_vapor_actual_mass_ratio().Value
        dry_air_denisty = self._calculate_dry_air_denisty().Value
        return Measurement(dry_air_denisty * (1 + humidity_ratio) /
                           (1 + (gas_constant_ratio * humidity_ratio)), "kg/m^3")

    def get_temperature(self):
        return self._temperature

    def get_kelvin_T(self):
        return self._temperature + 273.15

    def get_celsius_T(self):
        return self._temperature

    def get_pressure(self):
        return self._pressure

    def get_density(self):
        return self._density

    def update_temp_with_delta(self, delta_T):
        self._temperature += delta_T
