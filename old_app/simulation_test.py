from model.wall import *
from model.radiator import *
from model.room import *
from model.air import *
from model.outdoor import *

from controllers.P import *
from controllers.PI import *
from controllers.PD import *
from controllers.PID import *
from measurements import *
from simulation import *


initial_temperature = 22
set_temperature = 20

# simulation_time = 6
# simulation_time = 12
# simulation_time = 24
simulation_time = 48

outdoor = Outdoor('outdoor', temperatures)
ground = Outdoor('ground', ground_temperatures)

layers1 = [
    WallLayer(materials['concrete cinder'], 0.1),
    WallLayer(materials['spruce'], 0.05),
    WallLayer(materials['mica insulator'], 0.2),
    WallLayer(materials['brick'], 0.2),
]

layers2 = ['ytong1L40', 0.4]

layers3 = []

layers = layers1

walls = [
    Wall('nr1', 'S', 5, 4, outdoor, layers),
    Wall('nr2', 'S', 5, 4, outdoor, layers),
    Wall('nr3', 'L', 7, 4, outdoor, layers),
    Wall('nr4', 'L', 7, 4, outdoor, layers),
    Wall('ceiling', 'C', 5, 7, outdoor, layers),
    Wall('floor', 'F', 5, 7, ground, layers),
]

radiator = Radiator("r1", 1029, 70, 50, 1.3)

air = Air(initial_temperature, 10, 0.2)

room = Room(5, 7, 4, air)

for wall in walls:
    room.add_wall(wall)

room.add_radiator(radiator)

# Ziegler-Nichols tuning
T = 3000
K = 8
d = 150

P = (1.2 * T)/(K * d)
Ti = 2.0 * d
Td = 0.5 * d


# controller = PController(air, radiator, set_temperature, 2)
# controller = PIController(air, radiator, set_temperature, 2, 2 * 3600)
# controller = PDController(air, radiator, set_temperature, P, Td)
controller = PIDController(air, radiator, set_temperature, P, Ti, Td)

measurements = Measurements([ air, room, controller, radiator ])

simulation = Simulation([ outdoor, ground, *walls, radiator, room, air, controller, measurements ])

simulation.run(simulation_time * 3600)

measurements.print_stats()
measurements.show()
