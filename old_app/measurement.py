from collections import namedtuple


class Measurement(namedtuple('Measurement', ['Value', 'Unit'])):
    __slots__ = ()

    def __str__(self):
        return f"{round(self.Value, 8)} [{self.Unit}]"
