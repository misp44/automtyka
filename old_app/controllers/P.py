from controllers.controller import Controller


class PController(Controller):
    def __init__(self, input, output, setpoint, P):
        super().__init__(input, output, setpoint)

        self._P = P

    def calculate_gain(self, input):
        err = self._setpoint - input

        return self._P * err
