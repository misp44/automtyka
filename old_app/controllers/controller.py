from measurement import Measurement


class Controller(object):
	def __init__(self, input, output, setpoint):
		self._setpoint = setpoint
		self._input = input
		self._output = output
		self.measurements = {}

	def calculate(self, timestamp):
		out = self.calculate_gain(self._input.get_temperature())

		self._output.set_gain(out)

		self.measurements = {
			"controller gain": Measurement(out, 'amplification') 
		}

	def calculate_gain(self, input):
		return 0
