from controllers.controller import Controller


class PIController(Controller):
    def __init__(self, input, output, setpoint, P, Ti):
        super().__init__(input, output, setpoint)

        self._P = P
        self._Ti = Ti

        self._sum_of_errors = 0

    def calculate_gain(self, input):
        err = self._setpoint - input

        # Integral
        self._sum_of_errors += err # TODO: * dt ?

        return self._P * (err + self._sum_of_errors / self._Ti)
