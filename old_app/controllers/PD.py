from controllers.controller import Controller


class PDController(Controller):
    def __init__(self, input, output, setpoint, P, Td):
        super().__init__(input, output, setpoint)

        self._P = P
        self._Td = Td

        self._last_err = 0

    def calculate_gain(self, input):
        err = self._setpoint - input

        output = err - self._last_err # TODO: / dt ?

        self._last_err = err

        return self._P * (err + output * self._Td)

