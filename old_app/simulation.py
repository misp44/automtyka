import time
from measurement import Measurement


class Simulation(object):
    def __init__(self, objects):
        self._objects = objects

    def run(self, simulation_time):
        print("Running simulation...")

        start = time.time()

        for timeStamp in range(simulation_time):
            for o in self._objects:
                o.calculate(timeStamp)
        
        end = time.time()

        print(f"Simulation time {end - start} [s]")
