# Thermostat simulator

## Installing

```sh
pip3 install pipenv
```

```sh
pipenv install
```

## Run

```sh
pipenv shell
```

```sh
python3 simulation_test.py
```
