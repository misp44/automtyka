# Thermostat simulator

## Run

```sh
docker-compose build
```

```sh
docker-compose up
```

Application should be available on port `8080`.
