#!/bin/sh

if [ -z "$API_URL" ]; then
	echo "API_URL not defined"

	exit 1
fi

if [ -z "$GRAFANA_URL" ]; then
	echo "GRAFANA_URL not defined"

	exit 1
fi

sed -i "s~{{API_URL}}~${API_URL}~g" /usr/share/nginx/html/index.html
sed -i "s~{{GRAFANA_URL}}~${GRAFANA_URL}~g" /usr/share/nginx/html/index.html

nginx -g "daemon off;"
