import Vue from 'vue'

import Vuikit from 'vuikit'
import VuikitIcons from '@vuikit/icons'
import Vuelidate from 'vuelidate'
import '@vuikit/theme'

import router from './WizardRouter'
import App from './App.vue'

import store from './store'
import API, { APIPlugin } from './api'

Vue.use(Vuikit)
Vue.use(VuikitIcons)
Vue.use(APIPlugin)
Vue.use(Vuelidate)

Vue.config.productionTip = false

const api = new API(window.apiUrl)

new Vue({
	store,
	router,
	api,
	render: h => h(App),
}).$mount('#app')
