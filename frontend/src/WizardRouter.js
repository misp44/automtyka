import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export const steps = [
	{
		path: '/room',
		component: () => import(/* webpackChunkName: "rooms" */ './components/RoomParameters.vue'),
		meta: {
			title: 'Room'
		}
	},
	{
		path: '/radiator',
		component: () => import(/* webpackChunkName: "radiator" */ './components/RadiatorParameters.vue'),
		meta: {
			title: 'Radiator'
		}
	},
	{
		path: '/air',
		component: () => import(/* webpackChunkName: "air" */ './components/AirParameters.vue'),
		meta: {
			title: 'Air'
		}
	},
	{
		path: '/walls',
		component: () => import(/* webpackChunkName: "walls" */ './components/WallsParameters.vue'),
		meta: {
			title: 'Walls'
		}
	},
	{
		path: '/outdoor',
		component: () => import(/* webpackChunkName: "outdoor" */ './components/OutdoorParameters.vue'),
		meta: {
			title: 'Outdoor'
		},
	},
	{
		path: '/controller',
		component: () => import(/* webpackChunkName: "controller" */ './components/ControllerParameters.vue'),
		meta:{
			title: 'Controller'
		}
	},
	{
		path: '/simulation',
		component: () => import(/* webpackChunkName: "simulation" */ './components/SimulationParameters.vue'),
		meta: {
			title: 'Simulation'
		}
	},
].map( ( value, index ) => ( { ...value, meta: { ...value.meta, index } } ) );

export default new Router({
	mode: 'history',
	base: process.env.BASE_URL,
	routes: [
		...steps,
		{ path: '/', redirect: '/room' }
	]
})
