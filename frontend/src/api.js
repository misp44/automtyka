import axios from 'axios'
import { v4 as uuid } from 'uuid';

export default class API {
	constructor(url) {
		console.log( 'api url: ', url );

		this.url = url;
	}

	async startSimulation( parameters ) {
		const {
			room,
			radiator: {
				nominalHeatEmission,
				constant,
				inletTemperature
			},
			air: {
				initialTemperature,
				altitude,
				humidity
			},
			walls,
			outdoor: {
				outdoorTemperatures,
				groundTemperatures
			},
			controller: {
				setTemperature,
				proportional,
				integral,
				derivative
			},
			simulation: {
				time
			}
		} = parameters;

		const id = uuid();

		const mappedParameters = {
			simulation_id: id,
			room,
			walls: _mapWalls(walls),
			radiator: {
				nominal_heat_emission: nominalHeatEmission,
				radiator_constant: constant,
				inlet_temperature: inletTemperature
			},
			outdoor: {
				outdoor_temperatures: outdoorTemperatures,
				ground_temperatures: groundTemperatures
			},
			air: {
				initial_temperature: initialTemperature,
				altitude,
				humidity,
			},
			controller: {
				set_temperature: setTemperature,
				proportional,
				integral,
				derivative
			},
			time
		};

		await axios.post(`${this.url}/run`, mappedParameters);

		return id;
	}
}

let installed = false;

export class APIPlugin {
	static install(Vue) {
		if (installed)
			return

		installed = true

		let api

		Vue.mixin({
			beforeCreate () {
				api = this.$options.api || api
			}
		})

		Object.defineProperty(Vue.prototype, '$api', {
			get () { return api }
		})
	}
}

function _mapWalls(walls) {
	const mapLayer = ({ conductivity, thickness }) => ({conductivity, thickness});
	const mapEntry = ([ name, layers ]) => [ name, layers.map(mapLayer) ];

	const entries = Object.entries(walls).map(mapEntry);

	return Object.fromEntries(entries);
}
