const state = {
	initialTemperature: 22,
	altitude: 10,
	humidity: 0.2
};

const mutations = {
	updateInitialTemperature(state, temperature) {
		state.initialTemperature = Number.parseFloat(temperature)
	},
	updateAltitude(state, altitude) {
		state.altitude = Number.parseFloat(altitude)
	},
	updateHumidity(state, humidity) {
		state.humidity = Number.parseFloat(humidity)
	}
}

export default {
	namespaced: true,
	state,
	mutations
}
