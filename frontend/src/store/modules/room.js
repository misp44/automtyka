const state = {
	width: 5,
	depth: 7,
	height: 3
};

const mutations = {
	updateWidth(state, value) { state.width = Number.parseFloat(value) },
	updateDepth(state, value) { state.depth = Number.parseFloat(value) },
	updateHeight(state, value) { state.height = Number.parseFloat(value) }
}

export default {
	namespaced: true,
	state,
	mutations
}
