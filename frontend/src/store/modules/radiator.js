const state = {
	nominalHeatEmission: 1029,
	constant: 1.3,
	inletTemperature: 70
};

const mutations = {
	updateNominalHeatEmission(state, nominalHeatEmission) { state.nominalHeatEmission = Number.parseFloat(nominalHeatEmission) },
	updateConstant(state, value) { state.constant = Number.parseFloat(value) },
	updateInletTemperature(state, value) { state.inletTemperature = Number.parseFloat(value) }
}

export default {
	namespaced: true,
	state,
	mutations
}
