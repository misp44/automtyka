import data from './data/outdoor'

const state = {
	outdoorTemperatures: data.outdoorTemperatures(),
	groundTemperatures: data.groundTemperatures(),
	maxTemperature: 24,
	minTemperature: 12
};

const mutations = {
	updateMaxTemperature(state, value) {
		state.maxTemperature = Number.parseFloat(value) || 40;

		updateOutdoorTemperatures(state);
		updateGroundTemperatures(state);
	},

	updateMinTemperature(state, value) {
		state.minTemperature = Number.parseFloat(value) || -10;

		updateOutdoorTemperatures(state);
		updateGroundTemperatures(state);
	}
}

export default {
	namespaced: true,
	state,
	mutations
}

function updateOutdoorTemperatures(state) {
	const min = Math.min(...data.outdoorTemperatures())
	const max = Math.max(...data.outdoorTemperatures())

	const targetMin = state.minTemperature;
	const targetMax = state.maxTemperature

	state.outdoorTemperatures = data.outdoorTemperatures().map(
		t => Math.round(((t - min) / ( max - min )) * (targetMax - targetMin) + targetMin)
	)
}

function updateGroundTemperatures(state) {
	const min = Math.min(...data.outdoorTemperatures())
	const max = Math.max(...data.outdoorTemperatures())

	const targetMin = state.minTemperature;
	const targetMax = state.maxTemperature

	state.groundTemperatures = data.groundTemperatures().map(
		t => Math.round((( t - min ) / ( max - min )) * ( targetMax - targetMin ) + targetMin)
	)
}

