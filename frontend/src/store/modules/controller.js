const state = {
	setTemperature: 20,
	proportional: 3,
	integral: 300,
	derivative: 75
};

const mutations = {
	updateSetTemperature(state, value) { state.setTemperature = Number.parseFloat(value) },
	updateProportional(state, value) { state.proportional = Number.parseFloat(value) },
	updateIntegral(state, value) { state.integral = Number.parseFloat(value) },
	updateDerivative(state, value) { state.derivative = Number.parseFloat(value) }
}

export default {
	namespaced: true,
	state,
	mutations
}
