const data = {
	outdoorTemperatures: () => [
		24,
		23,
		18,
		17,
		16,
		15,
		15,
		20,
		20,
		15,
		14,
		13,
		13,
		12,
		13,
		14
	],
	groundTemperatures: () => [
		20,
		18,
		16,
		15,
		13,
		12,
		11,
		12,
		13,
		12,
		11,
		11,
		10,
		10,
		10,
		12
	]
};

export default data;