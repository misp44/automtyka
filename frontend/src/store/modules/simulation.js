const state = {
	time: 48
};

const mutations = {
	updateTime(state, time) {
		state.time = Number.parseFloat(time)
	}
}

export default {
	namespaced: true,
	state,
	mutations
}
