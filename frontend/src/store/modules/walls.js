const state = {
	left: defaultWallsLayers(),
	right: defaultWallsLayers(),
	front: defaultWallsLayers(),
	back: defaultWallsLayers(),
	ceiling: defaultWallsLayers(),
	floor: defaultWallsLayers()
};

const getters = {
	getLayersOfWall: state => wall => state[ wall ],
	getLayer: state => (wall, index) => state[ wall ][ index ],
	getLayersAmount: state => wall => state[ wall ].length
};

const mutations = {
	addLayer(state, { wall, name, conductivity, thickness }) {
		state[ wall ].push( { id: Math.random(), name, conductivity, thickness } );
	},

	removeLayer(state, { wall, index }) {
		state[ wall ].splice( index, 1 );
	},

	modifyLayer(state, { wall, index, ...entity }) {
		// TODO: Parse conductivity and thickness !!

		state[ wall ][ index ] = {
			...state[ wall ][ index ],
			...entity
		};
	},

	swapLayers(state, { wall, a, b }) {
		const layers = state[ wall ];
		const temporary  = layers[ b ];

		layers[ b ] = layers[ a ];
		layers[ a ] = temporary;
	}
}

export default {
	namespaced: true,
	state,
	getters,
	mutations
}

function defaultWallsLayers() {
	return [
		{ name: 'concrete cinder', conductivity: 0.335, thickness: 0.1 },
		{ name: 'spruce', conductivity: 0.126, thickness: 0.05 },
		{ name: 'mica isulator', conductivity: 0.121, thickness: 0.2 },
		{ name: 'brick', conductivity: 0.711, thickness: 0.2 }
	]
}
