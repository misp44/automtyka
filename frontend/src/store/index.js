import Vue from 'vue'
import Vuex from 'vuex'
import room from './modules/room'
import radiator from './modules/radiator'
import air from './modules/air'
import walls from './modules/walls'
import outdoor from './modules/outdoor'
import controller from './modules/controller'
import simulation from './modules/simulation'

Vue.use(Vuex)

export default new Vuex.Store({
	modules: {
		room,
		radiator,
		air,
		walls,
		outdoor,
		controller,
		simulation
	}
})


