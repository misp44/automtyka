function chartOptions(samples) {
	return {
		chart: {
			height: 350,
			type: 'line',
			toolbar: {
				show: false
			},
			animations: {
				enabled: false
			},
			zoom: {
				enabled: false
			}
		},
		colors: ['#77B6EA', '#545454'],
		dataLabels: {
			enabled: true,
		},
		stroke: {
			curve: 'smooth'
		},
		title: {
			text: 'Outdoor temperatures',
			align: 'left'
		},
		grid: {
			borderColor: '#e7e7e7',
			row: {
				colors: ['#f3f3f3', 'transparent'],
				opacity: 0.5
			},
		},
		markers: {
			size: 1
		},
		xaxis: {
			categories: new Array( samples ).fill().map((_, index) => index * 3),
			title: {
				text: 'Hour'
			}
		},
		yaxis: {
			title: {
				text: 'Temperature'
			},
			min: -20,
			max: 40
		},
		tooltip: {
			shared: false,
			y: {
				formatter: val => val.toFixed(1)
			}
		},
		legend: {
			position: 'top',
			horizontalAlign: 'right',
			floating: true,
			offsetY: -25,
			offsetX: -5
		}
	}
}

export default chartOptions;
